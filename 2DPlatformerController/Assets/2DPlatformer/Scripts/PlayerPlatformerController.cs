﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerPlatformerController : PhysicsObject {

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    private int ram;

    public Text winText;
    private SpriteRenderer spriteRenderer;
    private Animator animator;

    public GameObject turret;
    public GameObject shield;
    

    // Use this for initialization
    void Awake () 
    {
        // the player starts the game with no RAM (therefore can't control drones)
        ram = 0;
        turret.SetActive(false);
        shield.SetActive(false);
        spriteRenderer = GetComponent<SpriteRenderer> ();    
        animator = GetComponent<Animator> ();
        winText.text = "";
    }

    void Update(){
         targetVelocity = Vector2.zero;
        ComputeVelocity();
        if (ram >= 3)
        {
            winText.text = "You Win!!!";
        }
    }
    protected override void ComputeVelocity()
    {
        if(transform.position.y < -100)
        {
            RestartLevel();
        }
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis ("Horizontal");

        if (Input.GetButtonDown ("Jump") && grounded) {
            velocity.y = jumpTakeOffSpeed;
        } else if (Input.GetButtonUp ("Jump")) 
        {
            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.5f;
            }
        }

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
        if (flipSprite) 
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool ("grounded", grounded);
        animator.SetFloat ("velocityX", Mathf.Abs (velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }

    void OnTriggerEnter2D(Collider2D other)
	{
        // if the player collides with a ram game object
        if (other.gameObject.CompareTag("ram"))
        {
            other.gameObject.SetActive(false);
            ram = ram + 1;

            // turn on the turret
            if(ram >= 1)
            {
                turret.SetActive(true);
            }

            //turn on the shield
            if(ram >= 2)
            {
                shield.SetActive(true);
            }
        }
        
        // if the player collides with an enemy
        if (other.gameObject.CompareTag("Enemy"))
        {
            RestartLevel();
        }
	}
    
    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}