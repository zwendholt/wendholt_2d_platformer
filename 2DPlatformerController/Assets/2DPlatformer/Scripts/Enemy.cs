﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.right * Time.deltaTime * speed;
    }

     void OnTriggerEnter2D(Collider2D other)
	{
        // if the player collides with a ram game object
        if (other.gameObject.CompareTag("Projectile"))
        {
            Destroy(gameObject);
        }
        
        if (other.gameObject.CompareTag("Platform"))
        {
            TurnAround();
            Invoke("TurnAround", 2);
        }
    }
    void TurnAround()
    {
        speed *= -1;
    }
}
